import React from "react";
import { shallow } from "enzyme";
import GoogleMap from "components/Map/GoogleMap";

let component;

beforeEach(() => {
  component = shallow(<GoogleMap id="google-map" map={null} />);
});

describe("without map object", () => {
  it("doesnot has google map id", () => {
    expect(component.find("google-map").length).toEqual(0);
  });

  it("doesnot render without map", () => {
    expect(component).toMatchSnapshot();
  });
});
