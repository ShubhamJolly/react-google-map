import React from "react";
import { shallow } from "enzyme";
import Loader from "components/Loader/Loader";

let component;
describe("Loader doesnot renders when isLoading is false ", () => {
  beforeEach(() => {
    component = shallow(<Loader isLoading={false} />);
  });
  it("does not has loader-container", () => {
    expect(component.find(".loader-container").length).toEqual(0);
  });
  it("doesnot render", () => {
    expect(component).toMatchSnapshot();
  });
});

describe("Loader renders when isLoading is true ", () => {
  beforeEach(() => {
    component = shallow(<Loader isLoading={true} />);
  });
  it("has loader-container", () => {
    expect(component.find(".loader-container").length).toEqual(1);
  });
  it("renders correctly", () => {
    expect(component).toMatchSnapshot();
  });
});
