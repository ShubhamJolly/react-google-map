import React from "react";
import { shallow } from "enzyme";
import RouteDetail from "components/RouteDetail/RouteDetail";
let component;
describe("RouteDetail with empty direction object ", () => {
  beforeEach(() => {
    component = shallow(<RouteDetail direction={{}} />);
  });
  it("doesnot has col-lg-12 class", () => {
    expect(component.find(".col-lg-12").length).toEqual(0);
  });
  it("doesnot renders correctly", () => {
    expect(component).toMatchSnapshot();
  });
});

describe("RouteDetail with route object ", () => {
  beforeEach(() => {
    component = shallow(
      <RouteDetail direction={{ total_time: 1920, total_distance: 2000 }} />
    );
  });
  it("has col-lg-12 class", () => {
    expect(component.find(".col-lg-12").length).toEqual(2);
  });
  it("renders correctly", () => {
    expect(component).toMatchSnapshot();
  });
});
