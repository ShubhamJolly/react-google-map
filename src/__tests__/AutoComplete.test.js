import React from "react";
import { shallow } from "enzyme";
import AutoComplete from "components/AutoComplete/AutoComplete";
let component;
beforeEach(() => {
  const mapObj = null;
  component = shallow(
    <AutoComplete
      name="origin"
      onChangeInput={jest.fn()}
      placeholder="Enter a Start Location"
      map={mapObj}
      value="Karnal"
    />
  );
});
it("renders correclty with props", () => {
  expect(component).toMatchSnapshot();
});

it("has input textbox", () => {
  expect(component.find("input").length).toEqual(1);
});
