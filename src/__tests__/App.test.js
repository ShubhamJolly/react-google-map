import React from "react";
import { shallow } from "enzyme";
import App from "components/App/App";
import LocationSearch from "components/LocationSearch/LocationSearch";
import GoogleMap from "components/Map/GoogleMap";

let component;

beforeEach(() => {
  component = shallow(<App />);
});

it("renders App component correctly", () => {
  expect(component).toMatchSnapshot();
});

describe("renders without crashing", () => {
  describe("With map object  ", () => {
    beforeEach(() => {
      component.setState({
        map: {}
      });
    });

    it("shows Location Search with map object", () => {
      expect(component.find(LocationSearch).length).toEqual(1);
    });

    it("shows Google Map with map object", () => {
      expect(component.find(GoogleMap).length).toEqual(1);
    });
  });

  describe("With map null ", () => {
    beforeEach(() => {
      component.setState({
        map: null
      });
    });

    it("doesnot shows Location Search with map null", () => {
      expect(component.find(LocationSearch).length).toEqual(0);
    });

    it("doesnot shows Google Map with map null", () => {
      expect(component.find(GoogleMap).length).toEqual(0);
    });
  });
});
