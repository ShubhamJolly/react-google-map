import React from "react";
import { shallow } from "enzyme";
import CrossButton from "components/CrossButton/CrossButton";

let component;
describe("CrossButton with empty string", () => {
  beforeEach(() => {
    component = shallow(
      <CrossButton value="" name="destination" onChangeInput={jest.fn()} />
    );
  });

  it("doesnot has input-group-addon class with empty string", () => {
    expect(component.find(".input-group-addon").length).toEqual(0);
  });
  it("doesnot renders with empty string", () => {
    expect(component).toMatchSnapshot();
  });
});

describe("CrossButton with textbox value", () => {
  beforeEach(() => {
    component = shallow(
      <CrossButton value="cross" name="destination" onChangeInput={jest.fn()} />
    );
  });
  it("has input-group-addon class with textbox value", () => {
    expect(component.find(".input-group-addon").length).toEqual(1);
  });
  it("renders with textbox value", () => {
    expect(component).toMatchSnapshot();
  });
});
