import React from "react";
import { shallow } from "enzyme";
import App from "components/App/App";
import * as ApiHandler from "api/apiHandler";

let component;

beforeEach(() => {
  component = shallow(<App />);
});
describe("Mock API tests", () => {
  it("getToken() recieves a sample token", async () => {
    const sampleToken = { data: { token: "abcdefgh12" } };
    const apiHandlerObj = jest.spyOn(ApiHandler, "getToken");

    apiHandlerObj.mockImplementation(() => Promise.resolve(sampleToken));
    await component.instance().onLocationSubmit("origin", "destinaton");
    expect(component.state().token).toBe(sampleToken.data.token);
    apiHandlerObj.mockClear();
  });

  it("getToken() returns an error message in case of failed api request", async () => {
    const sampleError = { response: { data: "Internal Server Error" } };
    const apiHandlerObj = jest.spyOn(ApiHandler, "getToken");

    apiHandlerObj.mockImplementation(() => Promise.reject(sampleError));
    await component.instance().onLocationSubmit("origin", "destinaton");
    expect(component.state().errorMessage).toBe(sampleError.response.data);
    expect(component.state().isLoader).toBeFalsy();
    apiHandlerObj.mockClear();
  });

  it("getDirectionPath() should recieve JSON data ", async () => {
    component.setState({
      token: "abcdefgh12",
      retrylimit: 2
    });

    const sampleData = {
      data: {
        status: "success",
        path: [
          ["22.372081", "114.107877"],
          ["22.326442", "114.167811"],
          ["22.284419", "114.159510"]
        ],
        total_distance: 20000,
        total_time: 1800
      }
    };

    const apiHandlerObj = jest.spyOn(ApiHandler, "getRoutePath");

    apiHandlerObj.mockImplementation(() => Promise.resolve(sampleData));

    await component.instance().getDirectionPath(); // method being tested

    expect(component.state().routeDetail).toBe(sampleData.data);
    expect(component.state().path).toBe(sampleData.data.path);
    expect(component.state().isLoader).toBeFalsy();
    expect(component.state().retrylimit).toBe(2);
    apiHandlerObj.mockClear();
  });

  it("getDirectionPath() returns an error message in case of failed api request", async () => {
    component.setState({
      token: "abcdefgh12",
      retrylimit: 2
    });

    const sampleError = { response: { data: "Internal Server Error" } };
    const apiHandlerObj = jest.spyOn(ApiHandler, "getRoutePath");

    apiHandlerObj.mockImplementation(() => Promise.reject(sampleError));
    await component.instance().getDirectionPath();
    expect(component.state().errorMessage).toBe(sampleError.response.data);
    expect(component.state().isLoader).toBeFalsy();
    apiHandlerObj.mockClear();
  });

  it("getDirectionPath() returns Location not accessible by car", async () => {
    component.setState({
      token: "abcdefgh12",
      retrylimit: 2
    });

    const sampleError = {
      response: { data: "Location not accessible by car" }
    };
    const apiHandlerObj = jest.spyOn(ApiHandler, "getRoutePath");

    apiHandlerObj.mockImplementation(() => Promise.reject(sampleError));
    await component.instance().getDirectionPath();
    expect(component.state().errorMessage).toBe(sampleError.response.data);
    expect(component.state().isLoader).toBeFalsy();
    expect(component.state().retrylimit).toBe(2);
    apiHandlerObj.mockClear();
  });

  it('getDirectionPath() does recursion if status is "in progress"', async () => {
    component.setState({
      token: "abcdefgh12",
      retrylimit: 2
    });
    const apiHandlerObj = jest.spyOn(ApiHandler, "getRoutePath");
    const sampleData = { data: { status: "in progress" } };
    apiHandlerObj.mockImplementation(() => Promise.resolve(sampleData));

    // First time retry
    await component.instance().getDirectionPath();
    expect(component.state().errorMessage).toBe("");
    expect(component.state().isLoader).toBeFalsy();
    expect(component.state().retrylimit).toBe(1);

    // Second time retry
    await component.instance().getDirectionPath();
    expect(component.state().retrylimit).toBe(0);
    apiHandlerObj.mockClear();
  });
});
