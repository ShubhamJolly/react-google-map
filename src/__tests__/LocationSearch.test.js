import React from "react";
import { shallow } from "enzyme";
import LocationSearch from "components/LocationSearch/LocationSearch";
import AutoComplete from "components/AutoComplete/AutoComplete";
import RouteDetail from "components/RouteDetail/RouteDetail";
import CrossButton from "components/CrossButton/CrossButton";

let component;
const mockSubmit = jest.fn().mockReturnValue(1);
beforeEach(() => {
  component = shallow(
    <LocationSearch
      map={{}}
      onSubmitSearch={mockSubmit}
      direction={{}}
      errorMessage="errorMessage"
      onReset={mockSubmit}
    />
  );
});

describe("LocationSearch renders ", () => {
  it("have two AutoComplete textboxes", () => {
    expect(component.find(AutoComplete).length).toEqual(2);
  });

  it("have two buttons", () => {
    expect(component.find("button").length).toEqual(2);
  });

  it("has Cross button to clear autocomplete textbox", () => {
    expect(component.find(CrossButton).length).toEqual(2);
  });

  it("has route path details", () => {
    expect(component.find(RouteDetail).length).toEqual(1);
  });

  it("sets the state isSubmit on Submission", () => {
    component
      .find("button")
      .first()
      .simulate("click", { target: {}, preventDefault: jest.fn() });
    expect(component.state("isSubmit")).toBeTruthy();
  });

  it("calls the props onFormSubmit function", () => {
    component
      .find("button")
      .first()
      .simulate("click", { target: {}, preventDefault: jest.fn() });
    expect(component.instance().props.onSubmitSearch()).toBe(1);
  });

  describe("on Reset Click ", () => {
    beforeEach(() => {
      component
        .find("button")
        .at(1)
        .simulate("click");
    });
    it("unset the state isSubmit", () => {
      expect(component.state("isSubmit")).toBeFalsy();
    });

    it("set the state origin empty", () => {
      expect(component.state("origin")).toBe("");
    });

    it("set the state destination empty", () => {
      expect(component.state("destination")).toBe("");
    });

    it("set the state destination empty", () => {
      expect(component.state("destination")).toBe("");
    });

    it("calls the props onReset button", () => {
      expect(component.instance().props.onReset(1));
    });
  });
});
