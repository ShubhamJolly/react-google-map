import axios from "axios";
import { API_BASE_URL, API_ROUTE } from "config/apiConstant";

/**
 *@description Created Axios object with BaseUrl configuration
 */
export const axiosObject = axios.create({
  baseURL: `${API_BASE_URL}`
});

/**
 *@description  Fetch token from the api
 *
 * @param {String} start  origin point
 * @param {String} end    destination point
 */
export const getToken = (start, end) => {
  const body = { origin: start, destination: end };
  return axiosObject.post(`${API_ROUTE}`, body);
};

/**
 * @description Fetch Route details
 *
 * @param {String} token fetched token from the getToken API
 */
export const getRoutePath = token => {
  return axiosObject.get(`${API_ROUTE}/${token}`);
};
