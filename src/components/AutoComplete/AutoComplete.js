import React, { Component } from "react";
import PropTypes from "prop-types";
import "components/AutoComplete/AutoComplete.css";

class AutoComplete extends Component {
  autocompleteInput = React.createRef();
  autocomplete = null;

  /**
   * @description Handler for both change and place_change events
   *
   * @param {*} event   Either contains target object or null
   */
  handlePlaceChanged = (event = null) => {
    const { name, onChangeInput } = this.props;
    let value = "";
    if (event) {
      value = event.target.value;
    } else {
      const place = this.autocomplete.getPlace();
      value = place.formatted_address;
    }
    onChangeInput(value, name);
  };

  /**
   * @description Initialse autocomplete if the map object exists
   */
  componentDidMount() {
    const { map } = this.props;
    if (!this.autocomplete && map) {
      this.loadAutoCompleteTextBox(map);
    }
  }

  /**
   * @description Initialse text box with Map's AutoComplete obect and Add place_changed event listener
   *
   * @param {object} map    Google Map object instance
   */
  loadAutoCompleteTextBox = map => {
    this.autocomplete = new map.places.Autocomplete(
      this.autocompleteInput.current,
      { types: ["geocode"] }
    );
    this.autocomplete.addListener("place_changed", this.handlePlaceChanged);
  };

  render() {
    const { name, placeholder, value } = this.props;
    return (
      <input
        ref={this.autocompleteInput}
        type="text"
        className="form-control textbox-min-width"
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={this.handlePlaceChanged}
      />
    );
  }
}

AutoComplete.defaultProps = {
  placeholder: "Enter a Location",
  map: null
};

AutoComplete.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  onChangeInput: PropTypes.func.isRequired,
  map: PropTypes.any
};

export default AutoComplete;
