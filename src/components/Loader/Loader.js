import React from "react";
import PropTypes from "prop-types";
import "components/Loader/Loader.css";

/**
 *
 * @description Loader
 */
const Loader = props => {
  const { isLoading } = props;
  return (
    isLoading && (
      <div className="loader-container">
        <div className="loader" />
      </div>
    )
  );
};

Loader.propTypes = {
  isLoading: PropTypes.bool.isRequired
};

export default Loader;
