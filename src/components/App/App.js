import React, { Component } from "react";
import LocationSearch from "components/LocationSearch/LocationSearch";
import GoogleMap from "components/Map/GoogleMap";
import {
  SUCCESS,
  IN_PROGRESS,
  ERROR,
  API_RETRY_LIMIT,
  API_RETRY_TIME
} from "config/apiConstant";
import { getToken, getRoutePath } from "api/apiHandler";
import Loader from "components/Loader/Loader";
import "components/App/App.css";

class App extends Component {
  state = {
    token: "",
    routeDetail: {},
    errorMessage: "",
    map: null,
    path: [],
    isLoader: false,
    retrylimit: API_RETRY_LIMIT
  };

  /**
   *@description Fetch request token from the api
   *
   * @param {String} start     origin point
   * @param {String} end       destination point
   */
  onLocationSubmit = async (start, end) => {
    try {
      this.setState({
        token: false,
        isLoader: true,
        errorMessage: "",
        routeDetail: {},
        path: []
      });
      const response = await getToken(start, end);
      if (response && response.data && response.data.token) {
        this.setState({ token: response.data.token }, () => {
          this.getDirectionPath();
        });
      }
    } catch (error) {
      this.setState({ errorMessage: error.response.data, isLoader: false });
    }
  };

  /**
   *@description Reset all state properties
   */
  onFormReset = () => {
    this.setState({
      token: false,
      isLoader: false,
      errorMessage: "",
      routeDetail: {},
      path: []
    });
  };

  /**
   *@description  Fetch route details
   */
  getDirectionPath = async () => {
    const { token, retrylimit } = this.state;
    try {
      const result = await getRoutePath(token);
      if (result && result.data && result.data.status === SUCCESS) {
        this.setState({
          routeDetail: result.data,
          path: result.data.path,
          isLoader: false,
          retrylimit: API_RETRY_LIMIT
        });
      } else if (result && result.data && result.data.status === IN_PROGRESS) {
        if (retrylimit >= 1) {
          this.setState({ retrylimit: retrylimit - 1 }, () => {
            setTimeout(() => this.getDirectionPath(), API_RETRY_TIME);
          });
        } else {
          this.setState({
            errorMessage: "Server is busy, please try after some time.",
            isLoader: false,
            retrylimit: API_RETRY_LIMIT
          });
        }
      } else if (result && result.data && result.data.status === ERROR) {
        this.setState({
          errorMessage: result.data.error,
          isLoader: false,
          retrylimit: API_RETRY_LIMIT
        });
      }
    } catch (error) {
      this.setState({
        errorMessage: error.response.data,
        isLoader: false,
        retrylimit: API_RETRY_LIMIT
      });
    }
  };

  /**
   *@description Loading Google Map object and updating in the state
   */
  componentDidMount() {
    if (window.google) {
      return this._onMapLoad();
    }
    const GOOGLE_API_KEY = process.env.REACT_APP_GOOGLE_API_KEY;
    const GoogleMAPURL = `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}&libraries=places`;
    const scriptELement = document.createElement("script");
    scriptELement.type = "text/javascript";
    scriptELement.async = true;
    scriptELement.src = GoogleMAPURL;
    document.body.appendChild(scriptELement);

    scriptELement.addEventListener("load", () => {
      this.setState({ map: window.google.maps });
    });
  }

  render() {
    const { routeDetail, errorMessage, map, path, isLoader } = this.state;
    return (
      <div>
        <div className="col-xs-12 col-sm-4 col-lg-3 col-md-3 padding15">
          {map && (
            <LocationSearch
              onSubmitSearch={this.onLocationSubmit}
              direction={routeDetail}
              errorMessage={errorMessage}
              map={map}
              onReset={this.onFormReset}
            />
          )}
        </div>
        <div className="col-xs-12 col-sm-8 col-lg-9 col-md-9 map-container">
          {map && <GoogleMap id="google-map" map={map} path={path} />}
        </div>

        <Loader isLoading={isLoader} />
      </div>
    );
  }
}

export default App;
