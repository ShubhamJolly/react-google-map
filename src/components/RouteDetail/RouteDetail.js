import React from "react";
import PropTypes from "prop-types";

/**
 * @description RouteDetail renders if the direction object is not empty
 */
const RouteDetail = props => {
  const { direction } = props;
  if (Object.keys(direction).length === 0) {
    return null;
  }
  return (
    <div className="row">
      <div className="col-lg-12">{`Total Distance :${
        direction.total_distance
      }`}</div>
      <div className="col-lg-12">{`Total Time : ${direction.total_time}`}</div>
    </div>
  );
};

RouteDetail.propTypes = {
  direction: PropTypes.object.isRequired
};

export default RouteDetail;
