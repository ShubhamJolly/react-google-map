import React, { Component } from "react";
import PropTypes from "prop-types";
import "components/Map/GoogleMap.css";

class GoogleMap extends Component {
  mapInsatnce = null;
  directionsService = null;
  directionsDisplay = null;

  /**
   * @description Initialse mapinstance if the map object exists
   */
  componentDidMount() {
    const { map, id } = this.props;
    if (!this.mapInsatnce && map) {
      this.initMap(map, id);
    }
  }

  /**
   * @description Either display route on the map if path exists or reset the map dom instance
   *
   * @param {Object} prevProps
   */
  componentDidUpdate(prevProps) {
    const { path, map } = this.props;
    if (this.mapInsatnce && map) {
      if (path.length && prevProps.path !== path) {
        this.displayMapRoute(path, map);
      }
      if (!path.length) {
        this.resetMapRoute();
      }
    }
  }

  /***
   * @description Draw map on the DOM
   *
   * @param {String} id      ID on which map would be drawn
   * @param {Object} map     Google Map object instance
   */
  initMap = (map, id) => {
    const options = {
      zoom: 13,
      center: { lat: 27.2038, lng: 77.5011 },
      mapTypeControl: false,
      draggable: false
    };
    this.mapInsatnce = new map.Map(document.getElementById(id), options);
    this.mapDirectionsHandler(map);
  };

  /***
   * @description Initialise the Map Direction and Service handlers
   *
   * @param {Object} map      Google Map object instance
   */
  mapDirectionsHandler = map => {
    this.directionsService = new map.DirectionsService();
    this.directionsDisplay = new map.DirectionsRenderer();
  };

  /***
   * @description Draw points location which would be used to draw  path on the Map
   *
   * @param {Array} path      Geo-Location of the origin and destination points
   * @param {Object} map      Google Map object instance
   */
  generateMapRoutePath = (path, map) => {
    let route = {},
      originPoint = path[0],
      destinationPoint = path[path.length - 1];
    route.origin = new map.LatLng(originPoint[0], originPoint[1]);
    route.destination = new map.LatLng(
      destinationPoint[0],
      destinationPoint[1]
    );
    return route;
  };

  /***
   * @description Draw path on map on the basis of path object
   *
   * @param {Array} path      Geo-Location of the origin and destination points
   * @param {Object} map      Google Map object instance
   */
  displayMapRoute = (path, map) => {
    this.directionsDisplay.setMap(this.mapInsatnce);

    const { origin, destination } = this.generateMapRoutePath(path, map);
    const requestObject = {
      origin,
      destination,
      travelMode: "DRIVING"
    };

    this.directionsService.route(requestObject, (result, status) => {
      if (status === "OK") {
        this.directionsDisplay.setDirections(result);
      }
    });
  };

  /**
   * @description Reset the map on Direction Display Service instance
   *
   */
  resetMapRoute = () => {
    this.directionsDisplay.setMap(null);
  };

  render() {
    const { id } = this.props;
    return <div className="google-map" id={id} />;
  }
}

GoogleMap.defaultProps = {
  map: null,
  path: []
};

GoogleMap.propTypes = {
  id: PropTypes.string.isRequired,
  map: PropTypes.any,
  path: PropTypes.array
};

export default GoogleMap;
