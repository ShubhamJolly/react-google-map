import React, { Component } from "react";
import PropTypes from "prop-types";
import RouteDetail from "components/RouteDetail/RouteDetail";
import AutoComplete from "components/AutoComplete/AutoComplete";
import CrossButton from "components/CrossButton/CrossButton";
import "components/LocationSearch/LocationSearch.css";

class LocationSearch extends Component {
  state = {
    origin: "",
    destination: "",
    isSubmit: false
  };

  /**
   * @description handler used to  update states and initiate fetch api from props function
   *
   * @param {Object} event    contains the target  object
   */
  onFormSubmit = event => {
    event.preventDefault();
    const { onSubmitSearch } = this.props;
    const { origin, destination } = this.state;
    onSubmitSearch(origin, destination);
    this.setState({ isSubmit: true });
  };

  /**
   * @description handler used to initiatee parent state change and change isSubmit state
   *
   * @param {Object} event    contains the target  object
   */
  onFormReset = event => {
    const { onReset } = this.props;
    this.setState({ origin: "", destination: "", isSubmit: false });
    onReset();
  };

  /**
   * @description handler used to update state
   *
   * @param {String} value   string to be updated
   * @param {String} key     Key where update will occurs i.e origin, destination
   *
   */
  onChangePlace = (value, key) => {
    const { onReset } = this.props;
    this.setState({ [key]: value });
    onReset();
  };

  render() {
    const { isSubmit, origin, destination } = this.state;
    const { direction, errorMessage, map } = this.props;
    const isSubmitEnable = origin && destination;
    const isResetEnable = origin || destination;
    return (
      <form>
        <div className="form-group">
          <label className="control-label" htmlFor="origin">
            Starting Location
          </label>
          <div className="input-group max-width">
            <AutoComplete
              name="origin"
              onChangeInput={this.onChangePlace}
              placeholder="Enter a Start Location"
              map={map}
              value={origin}
            />
            <CrossButton
              value={origin}
              onChangeInput={this.onChangePlace}
              name="origin"
            />
          </div>
        </div>

        <div className="form-group">
          <label className="control-label" htmlFor="destination">
            Drop off Location
          </label>
          <div className="input-group max-width">
            <AutoComplete
              name="destination"
              onChangeInput={this.onChangePlace}
              placeholder="Enter a Destination Location"
              map={map}
              value={destination}
            />
            <CrossButton
              value={destination}
              onChangeInput={this.onChangePlace}
              name="destination"
            />
          </div>
        </div>

        <div className="form-group">
          <RouteDetail direction={direction} />
        </div>
        {errorMessage && (
          <div className="form-group text-danger">{errorMessage}</div>
        )}
        <div className="form-group">
          <button
            onClick={this.onFormSubmit}
            disabled={!isSubmitEnable}
            type="button"
            className="btn btn-primary button-margin"
          >
            {isSubmit ? "Re-Submit" : "Submit"}
          </button>
          <button
            onClick={this.onFormReset}
            disabled={!isResetEnable}
            type="button"
            className="btn btn-default "
          >
            Reset
          </button>
        </div>
      </form>
    );
  }
}

LocationSearch.defaultProps = {
  errorMessage: ""
};

LocationSearch.propTypes = {
  onSubmitSearch: PropTypes.func.isRequired,
  map: PropTypes.object.isRequired,
  errorMessage: PropTypes.string,
  direction: PropTypes.object.isRequired,
  onReset: PropTypes.func.isRequired
};

export default LocationSearch;
