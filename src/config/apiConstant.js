/**
 * API BASE URL & OTHER ROUTES
 */
export const API_BASE_URL = "https://mock-api.dev.lalamove.com";
export const API_ROUTE = "/route";
export const API_RETRY_LIMIT = 2;
export const API_RETRY_TIME = 3000;

/**
 * API STATUS
 */
export const SUCCESS = "success";
export const IN_PROGRESS = "in progress";
export const ERROR = "failure";
