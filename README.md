# Google Map Integration with React for Route Directions

This app is created to get the route directions from the google maps with the help of the Mock Apis, which would provide the points co-ordinates

Technologies Used:

* React
* Jest
* Enzymes
* ES 6
* GIT
* Boostrap 3

## Installation

```
npm install
```

Change MockApi Urls in src/config/apiConstant file

```
API_BASE_URL = <URL>
API_ROUTE = <URL>
```

Add a `.env` file with the following:

```
REACT_APP_GOOGLE_API_KEY = <api-key>
```

## Run Tests

```
npm test
```

## Run App in development mode

```
npm  start
```

## Build for production

```
npm run build
```
